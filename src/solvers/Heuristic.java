package solvers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import resourcesets.Problem;

/**
 * Heuristic is the base class for all heuristic methods.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public abstract class Heuristic {
  protected final HashMap<Integer, Set<Integer>> resources;
    // maps user index to indices of compatible available resource sets
  protected final HashMap<Integer, Set<Integer>> conflicts;
    // maps indices of available resources sets to indices of conflicting
    // available resource sets
  protected final HashMap<Integer, Set<Integer>> users;
    // maps indices of available resources sets to indices of unserved users
    // who can use the sets
  protected final int[] solution;  // indices of assigned resource sets
  protected final Random rng;      // random number generator for tie breaking
  protected int nServed;           // the number of users served by the solution
  protected final HashSet<Integer> resourceRemovalQueue;
    // resource sets to be removed during an iteration
  protected final HashSet<Integer> userRemovalQueue;
    // users to be removed during an iteration

  /**
   * Constructor.
   * @param problem the problem to solve
   * @param seed a random number seed
   */
  protected Heuristic(final Problem problem, final long seed) {
    int nUsers = problem.getNUsers();
    int nResources = problem.getNResourceSets();
    // Set up the random number generator.
    rng = new Random(seed);
    // Set up the solution vector.
    solution = new int[nUsers];
    // Set up the various maps.
    resources = new HashMap<>();
    conflicts = new HashMap<>();
    users = new HashMap<>();
    for (int i = 0; i < nUsers; i++) {
      resources.put(i, problem.getResourcesFor(i));
    }
    for (int j = 0; j < nResources; j++) {
      users.put(j, problem.getUsersFor(j));
      conflicts.put(j, problem.getConflictsWith(j));
    }
    // Set up the removal queues.
    userRemovalQueue = new HashSet<>();
    resourceRemovalQueue = new HashSet<>();
  }

  /**
   * Gets the number of users served by the solution.
   * @return the number of users served
   */
  public final int getValue() {
    return nServed;
  }

  /**
   * Gets the allocations made by the heuristic solution.
   * @return an array containing the index of the set assigned to each user
   * (or -1 if there was no assignment to that user)
   */
  public final int[] getSolution() {
    return Arrays.copyOf(solution, solution.length);
  }

  /**
   * Solves the problem.
   */
  public final void solve() {
    // Run the heuristic.
    run();
    // Calculate the number of customers served.
    nServed = (int) Arrays.stream(solution).filter(x -> x >= 0).count();
  }

  /**
   * Runs the specific heuristic.
   */
  abstract void run();

  /**
   * Iteratively removes users that can no longer be assigned a resource set
   * and resource sets that can no longer be used (either due to having been
   * used, being in conflict with a resource set in use, or no longer having
   * any users that would potentially need it).
   */
  protected final void purge() {
    // Loop until there is nothing left to do.
    while (!userRemovalQueue.isEmpty() || !resourceRemovalQueue.isEmpty()) {
      // Start by removing any queued users.
      for (int u : userRemovalQueue) {
        // For any surviving resource compatible with that user, remove the
        // user from the set of possible users of the resource.
        for (int r : resources.get(u)) {
          Set<Integer> z = users.get(r);
          z.remove(u);
          // If there is no user left for the resource, queue it for
          // removal.
          if (z.isEmpty()) {
            resourceRemovalQueue.add(r);
          }
        }
        // Remove the user from the resources map.
        resources.remove(u);
      }
      // Empty the user queue.
      userRemovalQueue.clear();
      // Make a copy of the resource removal queue and empty it (so that
      // new additions can be made cleanly).
      // Now remove any resource sets queued for removal.
      for (int r : resourceRemovalQueue) {
        // Remove r from consideration for any compatible users.
        for (int u : users.get(r)) {
          Set<Integer> z = resources.get(u);
          z.remove(r);
          // If the user has no viable resources left, queue the user
          // for removal.
          if (z.isEmpty()) {
            userRemovalQueue.add(u);
          }
        }
        users.remove(r);
        // The resource set being removed need no longer be considered when
        // checking for conflicts.
        for (int q : conflicts.get(r)) {
          conflicts.get(q).remove(r);
        }
        conflicts.remove(r);
      }
      resourceRemovalQueue.clear();
    }
  }

}
