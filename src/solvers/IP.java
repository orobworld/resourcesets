package solvers;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloObjective;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplex.Status;
import java.util.Arrays;
import resourcesets.Problem;

/**
 * IP implements an integer programming model for the allocation problem.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class IP implements AutoCloseable {
  private static final double HALF = 0.5;  // used to get values of binary vars

  private final Problem problem;  // the problem to solve.
  private final int nUsers;       // number of users
  private final int nSets;        // number of resource sets

  private final IloCplex cplex;   // the model instance
  private final IloIntVar[][] x;  // x[i][j] = 1 iff user i gets resource set j
  private final IloIntVar[] y;    // y[j] = 1 iff resource set j is used

  /**
   * Constructor.
   * @param prob the problem instance to solve
   * @throws IloException if the model cannot be built
   */
  public IP(final Problem prob) throws IloException {
    problem = prob;
    nUsers = problem.getNUsers();
    nSets = problem.getNResourceSets();

    // Create the model instance.
    cplex = new IloCplex();
    // Create the variables.
    x = new IloIntVar[nUsers][nSets];
    y = new IloIntVar[nSets];
    for (int j = 0; j < nSets; j++) {
      y[j] = cplex.boolVar("Use_set_" + j);
      for (int i = 0; i < nUsers; i++) {
        x[i][j] = cplex.boolVar("Assign_" + i + "_" + j);
        // Preclude incompatible assignments.
        if (!problem.isCompatible(i, j)) {
          x[i][j].setUB(0);
        }
      }
    }
    // The objective is to maximize the number of assignments made.
    IloObjective obj = cplex.addMaximize();
    for (int i = 0; i < nUsers; i++) {
      cplex.addToExpr(obj, cplex.sum(x[i]));
    }
    // Each user can be assigned at most one resource set.
    for (int i = 0; i < nUsers; i++) {
      cplex.addLe(cplex.sum(x[i]), 1.0, "Assignments_to_" + i);
    }
    // The sum of all assignments to a given set must equal the indicator
    // for use of that set.
    for (int j = 0; j < nSets; j++) {
      IloLinearNumExpr expr = cplex.linearNumExpr();
      for (int i = 0; i < nUsers; i++) {
        expr.addTerm(1.0, x[i][j]);
      }
      expr.addTerm(-1.0, y[j]);
      cplex.addEq(expr, 0, "Use_set_" + j);
    }
    // If two sets are incompatible, they cannot both be used.
    for (int j = 0; j < nSets; j++) {
      for (int k = j + 1; k < nSets; k++) {
        if (problem.conflict(j, k)) {
          cplex.addLe(cplex.sum(y[j], y[k]), 1.0, "Conflict_" + j + "_" + k);
        }
      }
    }
  }

  /**
   * Tries to solve the model.
   * @return the final solver status
   * @throws IloException if CPLEX blows up
   */
  public Status solve() throws IloException {
    cplex.solve();
    return cplex.getStatus();
  }

  /**
   * Gets the final objective value.
   * @return the objective value
   * @throws IloException if CPLEX has no solution
   */
  public double getValue() throws IloException {
    return cplex.getObjValue();
  }

  /**
   * Gets the optimal solution (if one exists).
   * @return an array containing the index of the set assigned to each user
   * (or -1 if there was no assignment to that user)
   * @throws IloException if CPLEX has no solution
   */
  public int[] getSolution() throws IloException {
    // Initialize the result to -1 (no assignment) for each user.
    int[] sol = new int[nUsers];
    Arrays.fill(sol, -1);
    // For each user, get the assignment values.
    for (int i = 0; i < nUsers; i++) {
      // Get the assignment values for user i.
      double[] xx = cplex.getValues(x[i]);
      for (int j = 0; j < nSets; j++) {
        if (xx[j] > HALF) {
          sol[i] = j;
          break;
        }
      }
    }
    return sol;
  }

  @Override
  public void close() {
    cplex.close();
  }

}
