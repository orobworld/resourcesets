package solvers;

import java.util.Arrays;
import java.util.Map.Entry;
import java.util.Set;
import resourcesets.Problem;

/**
 * ConflictHeuristic attempts to construct a solution by selecting at
 * each step the remaining resource set with the fewest remaining conflicts
 * and allocating it to an unserved user for whom it is compatible.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public class ConflictHeuristic extends Heuristic {

  /**
   * Constructor.
   * @param problem the problem to solve
   * @param seed a random number seed
   */
  public ConflictHeuristic(final Problem problem, final long seed) {
    super(problem, seed);
  }

  /**
   * Runs the heuristic.
   */
  @Override
  protected final void run() {
    // Mark all users unassigned to start.
    Arrays.fill(solution, -1);
    // Iterate until no resource sets are left.
    while (!users.isEmpty()) {
      // Select the resource set with the fewest remaining conflicts (breaking
      // ties randomly).
      double min = Double.MAX_VALUE;
      int set = -1;
      for (Entry<Integer, Set<Integer>> e : conflicts.entrySet()) {
        double x = e.getValue().size() + rng.nextDouble();
        if (x < min) {
          min = x;
          set = e.getKey();
        }
      }
      if (set < 0) {
        throw new IllegalStateException("Failed to choose a set??");
      }
      // Select a user compatible with the chosen set, showing preference
      // for the user with the fewest remaining choices (breaking ties
      // randomly).
      min = Double.MAX_VALUE;
      int user = -1;
      for (int u : users.get(set)) {
        double x = resources.get(u).size() + rng.nextDouble();
        if (x < min) {
          min = x;
          user = u;
        }
      }
      if (user < 0) {
        throw new IllegalStateException("Failed to choose a user??");
      }
      // Assign the set to the user.
      solution[user] = set;
      // Mark the user, the set and all conflicting sets for removal.
      userRemovalQueue.add(user);
      resourceRemovalQueue.add(set);
      resourceRemovalQueue.addAll(conflicts.get(set));
      // Purge removable items.
      purge();
    }
  }

}
