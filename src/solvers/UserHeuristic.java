package solvers;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import resourcesets.Problem;

/**
 * UserHeuristic attempts to construct a solution by selecting at
 * each step the unserved user with the fewest (but nonzero) remaining
 * compatible resource sets and allocating the resource set having the fewest
 * conflicts with remaining resource sets.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class UserHeuristic extends Heuristic {

  /**
   * Constructor.
   * @param problem the problem to solve
   * @param seed a random number seed
   */
  public UserHeuristic(final Problem problem, final long seed) {
    super(problem, seed);
  }


  /**
   * Runs the heuristic.
   */
  @Override
  void run() {
    // Mark all users unassigned to start.
    Arrays.fill(solution, -1);
    // Iterate until no resource sets are left.
    while (!users.isEmpty()) {
      // Select the unserved user with the fewest remaining options
      // (breaking ties randomly).
      double min = Double.MAX_VALUE;
      int user = -1;
      for (Map.Entry<Integer, Set<Integer>> e : resources.entrySet()) {
        double x = e.getValue().size() + rng.nextDouble();
        if (x < min) {
          min = x;
          user = e.getKey();
        }
      }
      if (user < 0) {
        throw new IllegalStateException("Failed to choose a user??");
      }
      // Select a resource set compatible with the chosen user, showing
      // preference for the set with the fewest remaining conflicts (breaking
      // ties randomly).
      min = Double.MAX_VALUE;
      int set = -1;
      for (int r : resources.get(user)) {
        double x = conflicts.get(r).size() + rng.nextDouble();
        if (x < min) {
          min = x;
          set = r;
        }
      }
      if (set < 0) {
        throw new IllegalStateException("Failed to choose a set??");
      }
      // Assign the set to the user.
      solution[user] = set;
      // Mark the user, the set and all conflicting sets for removal.
      userRemovalQueue.add(user);
      resourceRemovalQueue.add(set);
      resourceRemovalQueue.addAll(conflicts.get(set));
      // Purge removable items.
      purge();
    }
  }

}
