package resourcesets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.IntStream;

/**
 * Problem contains an instance of the resource assignment problem.
 *
 * There a number of users to be assigned resources. Not all will necessarily
 * receive assignments. The objective is to maximize the number that do.
 *
 * Each resource can be assigned to at most one user.
 *
 * Each user needs a specific number of resources, which must come from a
 * list of acceptable sets. Each acceptable set contains the correct number of
 * resources for that user, which must be consecutively numbered.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Problem {
  private final int nUsers;         // number of users
  private final int nResources;     // number of distinct resources
  private final int nResourceSets;  // number of distinct resource sequences
  private final int[] demand;       // demand for each user
  private final List<Set<Integer>> resourceSets;
    // master list of all resource sets suitable for at least one user
  private final Map<Set<Integer>, Integer> resourceIndex;
    // maps resource sets to their indices in the master list
  private final Map<Integer, Set<Integer>> compatibleSets;
    // maps each user number to the set of indices of compatible resource ranges
  private final Map<Integer, Set<Integer>> compatibleUsers;
    // maps each resource set to the set of users that can use it
  private final boolean[][] conflict;
    // conflict[i][j] is true iff the resource sets with indices i and j
    // intersect

  /**
   * Generates a random problem instance.
   * @param nU the number of users
   * @param nR the number of resources
   * @param maxR the maximum number of resources any user might need
   * @param p probability an arbitrary resource set of proper size will be
   * compatible for a particular user
   * @param seed the seed for a random number generator
   */
  public Problem(final int nU, final int nR, final int maxR,
                 final double p, final long seed) {
    nUsers = nU;
    nResources = nR;
    resourceSets = new ArrayList<>();
    resourceIndex = new HashMap<>();
    compatibleSets = new HashMap<>();
    compatibleUsers = new HashMap<>();
    demand = new int[nUsers];
    // Seed the random number generator.
    Random rng = new Random(seed);
    // Generate data for each user.
    for (int u = 0; u < nUsers; u++) {
      // Set the user's demand.
      int d = rng.nextInt(1, maxR);
      demand[u] = d;
      // Create an entry in the list of compatible sets for this user.
      HashSet<Integer> c = new HashSet<>();
      compatibleSets.put(u, c);
      // For each possible resource sequence of that length, decide randomly
      // whether the sequence will be compatible.
      for (int s = 0; s <= nResources - d; s++) {
        if (rng.nextDouble() < p) {
          // The sequence from s to s + d - 1 is a compatible resource for this
          // user.
          HashSet<Integer> r = makeRange(s, s + d);
          // Find its index or add it to the master resource collection.
          int ix = getRangeIndex(r);
          // Add the sequence to the compatible set for this user.
          c.add(ix);
        }
      }
      // Ensure that the user has at least one compatible resource allocation.
      if (c.isEmpty()) {
        // Choose a random starting point.
        int s = rng.nextInt(nResources - d);
        // Generate the corresponding resource set.
        HashSet<Integer> r = makeRange(s, s + d);
        // Find the index of the resource set and add it to the user set.
        c.add(getRangeIndex(r));
      }
    }
    nResourceSets = resourceSets.size();
    conflict = new boolean[nResourceSets][nResourceSets];
    // Generate the conflict map.
    for (int i = 0; i < nResourceSets; i++) {
      Set<Integer> s = resourceSets.get(i);
      for (int j = i + 1; j < nResourceSets; j++) {
        if (resourceSets.get(j).stream().anyMatch(x -> s.contains(x))) {
          conflict[i][j] = true;
          conflict[j][i] = true;
        }
      }
    }
    // Map resource sets back to the set of users that can use them.
    for (int i = 0; i < nResourceSets; i++) {
      compatibleUsers.put(i, new HashSet<>());
    }
    for (int i = 0; i < nUsers; i++) {
      for (int j : compatibleSets.get(i)) {
        compatibleUsers.get(j).add(i);
      }
    }
  }

  /**
   * Generates a resource range.
   * @param lo the index of the first included resource
   * @param hi the index of the first excluded resource
   * @return the sequence of resource indices as a set
   */
  private HashSet<Integer> makeRange(final int lo, final int hi) {
    return new HashSet<>(IntStream.range(lo, hi).boxed().toList());
  }

  /**
   * Gets the index of a resource set, adding the set to the master records
   * if not already present.
   * @param resource a resource set
   * @return the index of the resource set
   */
  private int getRangeIndex(final HashSet<Integer> resource) {
    int ix;
    if (resourceIndex.containsKey(resource)) {
      ix = resourceIndex.get(resource);
    } else {
      ix = resourceSets.size();
      resourceSets.add(resource);
      resourceIndex.put(resource, ix);
    }
    return ix;
  }

  /**
   * Gets a string representation of the problem.
   * @return a string summarizing the problem.
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("There are ").append(nUsers).append(" users and ")
      .append(nResources).append(" resources.\n");
    for (int u = 0; u < nUsers; u++) {
      sb.append("Compatible allocations for user ").append(u)
        .append(" (demand ").append(demand[u]).append("):\n");
      compatibleSets.get(u).stream().sorted()
                .forEach(z -> sb.append("\t").append(resourceSets.get(z))
                                .append("\n"));
    }
    return sb.toString();
  }

  /**
   * Gets the number of users.
   * @return the number of users
   */
  public int getNUsers() {
    return nUsers;
  }

  /**
   * Gets the number of distinct resources sets.
   * @return the number of resource sets
   */
  public int getNResourceSets() {
    return nResourceSets;
  }

  /**
   * Tests if a resource set is compatible with a user.
   * @param user the user index
   * @param set the resource set index
   * @return true if the user can be assigned that set
   */
  public boolean isCompatible(final int user, final int set) {
    return compatibleSets.get(user).contains(set);
  }

  /**
   * Tests if two resource sets conflict (intersect).
   * @param i the index of the first set
   * @param j the index of the second set
   * @return true if the two sets intersect
   */
  public boolean conflict(final int i, final int j) {
    return conflict[i][j];
  }

  /**
   * Converts a solution to a string.
   * @param sol a vector giving the index of the allocated resource set for
   * each user (-1 if no set is allocated to that user)
   * @return a string summarizing the solution
   */
  public String decodeSolution(final int[] sol) {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < nUsers; i++) {
      sb.append("User ").append(i);
      if (sol[i] < 0) {
        sb.append(" receives no allocation\n");
      } else {
        sb.append(" is allocated set ").append(resourceSets.get(sol[i]))
          .append("\n");
      }
    }
    return sb.toString();
  }

  /**
   * Tests the validity of a solution.
   * If the solution is incorrect, an error message is printed as a side effect.
   * @param sol a vector giving the index of the allocated resource set for
   * each user (-1 if no set is allocated to that user)
   * @param count the number of users allegedly served
   * @return true if the solution is valid
   */
  public boolean testSolution(final int[] sol, final int count) {
    // Check that the number of users is correct.
    if (sol.length != nUsers) {
      System.out.println("The solution has the wrong dimension.");
      return false;
    }
    // Check that every assignment is a valid resource set for that user,
    // that no conflicts occur, and that no resource set is used twice.
    // Also count the number of users served.
    HashSet<Integer> used = new HashSet<>();
    int nServed = 0;
    for (int i = 0; i < nUsers; i++) {
      int s = sol[i];
      if (s >= 0) {
        // Check for reuse.
        if (used.contains(s)) {
          System.out.println("Resource set " + s + " is being reused.");
          return false;
        }
        // Check validity.
        if (!compatibleSets.get(i).contains(s)) {
          System.out.println("Incompatible resource set " + s
                             + " is being assigned to user " + i + ".");
          return false;
        }
        // Check for a conflict.
        for (int t : used) {
          if (conflict[s][t]) {
            System.out.println("Assigning resource " + s
                               + " which conflicts with already assigned"
                               + " resource " + t + ".");
            return false;
          }
        }
        // Count the user as served.
        nServed += 1;
      }
    }
    // Check the objective value.
    if (nServed != count) {
      System.out.println("Discrepancy between reported objective value "
                         + count + " and actual number of customers served "
                         + nServed + ".");
      return false;
    }
    return true;
  }

  /**
   * Gets the set of indices of resources compatible with a particular user.
   * @param user the user index
   * @return the set of indices of compatible resource sets
   */
  public Set<Integer> getResourcesFor(final int user) {
    return new HashSet<>(compatibleSets.get(user));
  }

  /**
   * Gets the set of indices of users who can use a particular resource set.
   * @param resource the index of the resource set
   * @return the set of indices of compatible users
   */
  public Set<Integer> getUsersFor(final int resource) {
    return new HashSet<>(compatibleUsers.get(resource));
  }

  /**
   * Gets the set of indices of resources sets that conflict with a given set.
   * @param resource the index of the given resource set
   * @return the set of indices of conflicting resource sets
   */
  public Set<Integer> getConflictsWith(final int resource) {
    HashSet<Integer> s = new HashSet<>();
    for (int j = 0; j < nResourceSets; j++) {
      if (conflict[resource][j]) {
        s.add(j);
      }
    }
    return s;
  }
}
