package resourcesets;

import ilog.concert.IloException;
import ilog.cplex.IloCplex.Status;
import solvers.ConflictHeuristic;
import solvers.Heuristic;
import solvers.IP;
import solvers.UserHeuristic;

/**
 * ResourceSets investigates heuristics for a resource assignment problem
 * posted on OR Stack Exchange.
 * Original source:
 * <pre>
 * See <a href="https://or.stackexchange.com/questions/
   8435/how-to-model-this-user-packing-problem">Source post</a>
 * </pre>
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class ResourceSets {

  /**
   * Dummy constructor.
   */
  private ResourceSets() { }

  /**
   * Generates a test problem and finds both optimal and heuristic solutions.
   * @param args the command line arguments (unused)
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {
    // The user sets the problem dimensions and the random seed for the
    // problem generator here. (Random seeds for the heuristics are set below.)
    int nUsers = 20;            // the number of users in the system
    int nResources = 100;       // the number of resources in the system
    int maxResources = 16;      // the maximum demand of any single user
    double pAccept = 0.1;       // the probability that an arbitrary resource
                                // sequence of the proper size will be
                                // acceptable to a user
    long seed = 48823;          // the random seed for the problem generator
    // Create a problem instance.
    Problem prob = new Problem(nUsers, nResources, maxResources, pAccept, seed);
//    System.out.println(prob);
    // Try solving the IP model for an optimal solution.
    long time = System.currentTimeMillis();
    try (IP ip = new IP(prob)) {
      Status status = ip.solve();
      time = System.currentTimeMillis() - time;
      System.out.println("\nOverall MIP time including model building = "
                         + time + " ms.");
      System.out.println("Final solver status = " + status);
      if (status == Status.Optimal) {
        int n = (int) ip.getValue();
        System.out.println("Number of users receiving assignments = " + n);
        if (prob.testSolution(ip.getSolution(), n)) {
          System.out.println("Solution has been validated.");
          // Uncomment the next line to print the solution.
//          System.out.println(prob.decodeSolution(ip.getSolution()));
        }
      }
    } catch (IloException ex) {
      System.out.println("CPLEX threw an exception:\n" + ex.getMessage());
    }
    // Try the conflict heuristic.
    System.out.println("\nTrying minimum conflict greedy heuristic ...");
    // Set the seed for the conflict heuristic here.
    seed = 987;
    time = System.currentTimeMillis();
    Heuristic ch = new ConflictHeuristic(prob, seed);
    ch.solve();
    time = System.currentTimeMillis() - time;
    System.out.println("Overall time including model building = "
                       + time + " ms.");
    int n = ch.getValue();
    System.out.println("Conflict heuristic solution serves " + n + " users.");
    if (prob.testSolution(ch.getSolution(), n)) {
      System.out.println("Solution has been validated.");
      // Uncomment the next line to print the solution.
//      System.out.println(prob.decodeSolution(ch.getSolution()));
    }
    // Try the user heuristic.
    System.out.println("\nTrying the user greedy heuristic ...");
    // Set the seed for the user heuristic here.
    seed = 295;
    time = System.currentTimeMillis();
    Heuristic uh = new UserHeuristic(prob, seed);
    uh.solve();
    time = System.currentTimeMillis() - time;
    System.out.println("Overall time including model building = "
                       + time + " ms.");
    n = uh.getValue();
    System.out.println("User heuristic solution serves " + n + " users.");
    if (prob.testSolution(uh.getSolution(), n)) {
      System.out.println("Solution has been validated.");
      // Uncomment the next line to print the solution.
//      System.out.println(prob.decodeSolution(uh.getSolution()));
    }
  }

}
