# Resource Sets #

### What is this repository for? ###

This repository contains Java code to solve a [resource allocation problem](https://or.stackexchange.com/questions/8435/how-to-model-this-user-packing-problem) posted on Operations Research Stack Exchange.

### Details ###

The problem is fairly straightforward to state. There are $`N`$ resources to be shared among $`K`$ users. Each resource can be allocated to at most one user. Not all users will necessarily be served, and the objective is to maximize the number of users served. What makes the problem a bit interesting is that each user needs a specified number of units of resource, and those units *must be consecutive*. Not all consecutive blocks of resources of the correct size will necessarily work, though. So, for instance, a user with a demand of 3 might accept resource sets $`\lbrace 1, 2, 3\rbrace`$ or $`\lbrace 6, 7, 8\rbrace`$ but not $`\lbrace 1, 3, 4\rbrace`$ (not consecutive) and perhaps not $`\lbrace 4, 5, 6\rbrace`$ (consecutive but not suitable for some reason).

The code here generates a random problem instance, based on dimensions set by the user, and then finds an optimal solution by using solving an integer linear program (using CPLEX). It then tests two different heuristics, described in my  [blog post](https://orinanobworld.blogspot.com/2022/05/allocating-resource-sets.html). Other than CPLEX, no external libraries are required. (If the integer program is stripped out, the code should run without having CPLEX installed.)

### License ###

The code here is copyrighted by Paul A. Rubin (yours truly) and licensed under a [Creative Commons Attribution 3.0 Unported License](http://creativecommons.org/licenses/by/3.0/deed.en_US). 

